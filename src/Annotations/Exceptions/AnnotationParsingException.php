<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace MVQN\Annotations\Exceptions;

use Exception;

/**
 * Class AnnotationParsingException
 *
 * @package MVQN\Annotations\Exceptions
 * @author Ryan Spaeth <rspaeth@mvqn.net
 * @final
 */
final class AnnotationParsingException extends Exception
{
}