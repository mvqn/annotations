<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace MVQN\Annotations\Exceptions;

use Exception;

/**
 * Class AnnotationDeclarationException
 *
 * @package MVQN\Annotations\Exceptions
 * @author Ryan Spaeth <rspaeth@mvqn.net
 * @final
 */
final class AnnotationDeclarationException extends Exception
{
}